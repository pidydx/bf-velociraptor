#!/bin/bash

set -e

if [ "$1" = 'velociraptor' ]; then
    velociraptor --config=/etc/velociraptor/server.config.yaml config client > /tmp/client.config.yaml
    mkdir -p /var/lib/velociraptor/clients/{linux,windows,darwin}

    velociraptor config repack --exe /usr/local/share/velociraptor/clients/linux/velociraptor_client /tmp/client.config.yaml /var/lib/velociraptor/clients/linux/velociraptor_client
    velociraptor config repack --exe /usr/local/share/velociraptor/clients/darwin/velociraptor_client /tmp/client.config.yaml /var/lib/velociraptor/clients/darwin/velociraptor_client
    velociraptor config repack --exe /usr/local/share/velociraptor/clients/windows/velociraptor_client.exe /tmp/client.config.yaml /var/lib/velociraptor/clients/windows/velociraptor_client.exe

    velociraptor user add vradmin ${VRADMIN_PASS} --role administrator --config=/etc/velociraptor/server.config.yaml
    exec velociraptor frontend --config=/etc/velociraptor/server.config.yaml -v
fi

exec "$@"