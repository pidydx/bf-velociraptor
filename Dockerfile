#########################
# Create base container #
#########################
FROM ubuntu:22.04 as base
LABEL maintainer="pidydx"

# Set app user and group
ENV APP_USER=velociraptor
ENV APP_GROUP=velociraptor

# Set version pins
ENV VELOCIRAPTOR_VERSION=0.7.0

# Set base dependencies
ENV BASE_DEPS ca-certificates

# Set build dependencies
ENV BUILD_DEPS build-essential curl git gcc-multilib gcc-mingw-w64 wget

# Create app user and group
RUN groupadd -g 1000 ${APP_GROUP} \
 && useradd -r -M -N -u 1000 ${APP_USER} -g ${APP_GROUP} -s /usr/sbin/nologin

# Update and install base dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_DEPS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create build container #
##########################
FROM base AS builder

RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS}

ENV NODE_MAJOR_VERSION="18"
RUN curl -fsSL https://deb.nodesource.com/setup_${NODE_MAJOR_VERSION}.x | bash - \
 && apt-get install -y nodejs

ENV GO_VERSION="1.19.5"
RUN wget -nv https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz \
 && tar -C /usr/src -xzf go${GO_VERSION}.linux-amd64.tar.gz
ENV GOPATH=/usr/src/go
ENV PATH=$PATH:$GOPATH/bin

# Run build
COPY build.sh /usr/src/build.sh
WORKDIR /usr/src
RUN ./build.sh


###########################
## Create final container #
###########################
FROM base

# Finalize install
COPY etc/ /etc/
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

RUN update-ca-certificates \
 && mkdir -p /var/lib/velociraptor \
 && chown ${APP_USER}:${APP_GROUP} /var/lib/velociraptor

# Prepare container
EXPOSE 8000/tcp 8001/tcp 8889/tcp
VOLUME ["/etc/velociraptor", "/var/lib/velociraptor"]

USER $APP_USER

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["velociraptor"]
