#!/bin/bash

set -e

git clone https://github.com/Velocidex/velociraptor.git --depth=1 --branch v${VELOCIRAPTOR_VERSION}
cd velociraptor

cd gui/velociraptor/
npm install

make build
cd ../..
make linux
mv output/velociraptor* /usr/local/bin/velociraptor
mkdir -p /usr/local/share/velociraptor/clients/{linux,windows,darwin}
cp /usr/local/bin/velociraptor /usr/local/share/velociraptor/clients/linux/velociraptor_client
make windows
mv output/velociraptor* /usr/local/share/velociraptor/clients/windows/velociraptor_client.exe
make darwin
mv output/velociraptor* /usr/local/share/velociraptor/clients/darwin/velociraptor_client
